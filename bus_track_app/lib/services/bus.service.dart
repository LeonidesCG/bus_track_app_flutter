import 'dart:convert';

import 'package:bus_track_app/helpers/config.dart';
import 'package:http/http.dart' as http;
import 'package:bus_track_app/models/bus.dart';

class BusService {
  Future<Bus> getBusById(String busId) async {
    var body = <String, String>{'busId': busId};
    final response = await http.post(Uri.parse(Config.API_URL + 'bus/find'),
        headers: Config.HEADERS, body: jsonEncode(body));

    return Bus.fromJson(jsonDecode(response.body));
  }

  Future<List<Bus>> getBusesByRoute(String routeId) async {
    var body = <String, dynamic>{'active': true, 'route': routeId};
    final response = await http.post(Uri.parse(Config.API_URL + 'bus/find'),
        headers: Config.HEADERS, body: jsonEncode(body));

    var busesRawList = jsonDecode(response.body) as List<dynamic>;
    List<Bus> busesList = [];
    busesRawList.forEach((br) {
      busesList.add(Bus.fromJson(br));
    });
    return busesList;
  }
}
