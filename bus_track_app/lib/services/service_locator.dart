import 'package:bus_track_app/services/bus.service.dart';
import 'package:get_it/get_it.dart';

GetIt servicerLocator = GetIt.instance;

setupServiceLocator() {
  servicerLocator.registerLazySingleton<BusService>(() => BusService());
}
