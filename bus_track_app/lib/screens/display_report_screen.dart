import 'package:bus_track_app/constants/colors.dart';
import 'package:bus_track_app/helpers/config.dart';
import 'package:bus_track_app/models/reports.dart';
import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

// ignore: must_be_immutable
class DisplayReportScreen extends StatelessWidget {
  static const routeName = '/display-report-screen';
  var imgUrl = apiUrl + 'public/reports/';
  var phoneWidth;
  var halfPhoneWidth;
  var phoneHeight;
  var phoneThridHeight;
  Report data;
  DisplayReportScreen({this.data});

  var contenedor = new Padding(
    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
    child: ClipOval(
      child: new Image(
        image: AssetImage("assets/images/imagenNoDisponible.png"),
        height: 70,
        width: 70,
        fit: BoxFit.cover,
      ),
    ),
  );

  todayDate(e) {
    DateTime dt = DateTime.parse(e);
    String formattedTime = DateFormat('hh:mm a').format(dt);
    var formatter = new DateFormat('dd-MMM-yyyy');
    String formattedDate = formatter.format(dt);
    return formattedTime + '  ' + formattedDate;
  }

  @override
  Widget build(BuildContext context) {
    phoneWidth = MediaQuery.of(context).size.width;
    halfPhoneWidth = phoneWidth / 2;
    phoneHeight = MediaQuery.of(context).size.height;
    phoneThridHeight = phoneHeight / 3;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Informacion de tu reporte',
          style: GoogleFonts.openSans(
            textStyle: TextStyle(
              fontFamily: 'Open Sans',
              fontSize: bodyFont,
            ),
          ),
        ),
        backgroundColor: vinoTinto,
      ),
      body: ListView(
        children: [
          Wrap(
            alignment: WrapAlignment.start,
            children: [
              Stack(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          children: [
                            SizedBox(
                              width: halfPhoneWidth + halfPhoneWidth / 2,
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.directions_bus_sharp,
                                    color: Colors.grey[900],
                                  ),
                                  Text(
                                    'Autobus',
                                    style: GoogleFonts.openSans(
                                      textStyle: TextStyle(
                                        fontFamily: 'Open Sans',
                                        fontSize: bodyFont,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              width: halfPhoneWidth + halfPhoneWidth / 2,
                              child: Text(
                                data.bus.number.toString(),
                                style: GoogleFonts.openSans(
                                  textStyle: TextStyle(
                                    fontFamily: 'Open Sans',
                                    fontSize: bodyFont,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            SizedBox(
                              width: halfPhoneWidth + halfPhoneWidth / 2,
                              child: Row(
                                children: [
                                  Icon(
                                    CommunityMaterialIcons.chart_line_variant,
                                    color: Colors.black,
                                    // size: 40,
                                  ),
                                  Text(
                                    'Ruta',
                                    style: GoogleFonts.openSans(
                                      textStyle: TextStyle(
                                        fontFamily: 'Open Sans',
                                        fontSize: bodyFont,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              width: halfPhoneWidth + halfPhoneWidth / 2,
                              child: Text(
                                data.route.name,
                                style: GoogleFonts.openSans(
                                  textStyle: TextStyle(
                                    fontFamily: 'Open Sans',
                                    fontSize: bodyFont,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
            child: Container(
              child: data.description != null && data.description != ''
                  ? Column(
                      children: [
                        Row(
                          children: <Widget>[
                            Icon(
                              Icons.description_outlined,
                              color: Colors.grey[900],
                            ),
                            Text(
                              'Descripcion',
                              style: GoogleFonts.openSans(
                                textStyle: TextStyle(
                                  fontFamily: 'Open Sans',
                                  fontSize: bodyFont,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            data.description,
                            style: GoogleFonts.openSans(
                              textStyle: TextStyle(
                                fontFamily: 'Open Sans',
                                fontSize: bodyFont,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                      ],
                    )
                  : Text(
                      'El reporte no tiene descripcion.',
                      style: TextStyle(
                          fontSize: bodyFont, fontWeight: FontWeight.bold),
                    ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 5.0, right: 5.0),
            child: Divider(
              color: Colors.grey[700],
              height: 1,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          data.imgName != null
              ? Padding(
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                    child: new Image(
                      image: new NetworkImage(
                        '$imgUrl${data.imgName}',
                      ),
                      height: phoneThridHeight,
                      width: halfPhoneWidth,
                      fit: BoxFit.cover,
                    ),
                  ),
                )
              : contenedor,
          Padding(
            padding: const EdgeInsets.fromLTRB(5, 5, 5, 10),
            child: Align(
              alignment: Alignment.centerLeft,
              child: SizedBox(
                width: phoneWidth,
                height: 30,
                child: Text(
                  todayDate(data.date),
                  style: GoogleFonts.openSans(
                    textStyle: TextStyle(
                      fontFamily: 'Open Sans',
                      fontSize: bodyFont,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 5.0, right: 5.0),
            child: Divider(
              color: Colors.grey[700],
              height: 1,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Container(
              child: data.response != null && data.response.text != ''
                  ? Column(
                      children: [
                        Row(
                          children: <Widget>[
                            Icon(
                              Icons.question_answer,
                              color: Colors.grey[900],
                            ),
                            Text(
                              'Respuesta',
                              style: GoogleFonts.openSans(
                                textStyle: TextStyle(
                                  fontFamily: 'Open Sans',
                                  fontSize: bodyFont,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            data.response.text,
                            style: GoogleFonts.openSans(
                              textStyle: TextStyle(
                                fontFamily: 'Open Sans',
                                fontSize: bodyFont,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                      ],
                    )
                  : Text(
                      'Aun no tienes respuesta',
                      style: GoogleFonts.openSans(
                        textStyle: TextStyle(
                          fontFamily: 'Open Sans',
                          fontSize: bodyFont,
                        ),
                      ),
                    ),
            ),
          ),
        ],
      ),
    );
  }
}
