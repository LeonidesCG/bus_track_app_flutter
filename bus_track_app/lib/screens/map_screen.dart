import 'dart:async';
import 'package:bus_track_app/constants/colors.dart';
import 'package:bus_track_app/helpers/config.dart';
import 'package:bus_track_app/helpers/maps_helper.dart';
import 'package:bus_track_app/helpers/socket_helper.dart';
import 'package:bus_track_app/main.dart';
import 'package:bus_track_app/models/bus.dart';
import 'package:bus_track_app/models/busPosition.dart';
import 'package:bus_track_app/models/globals.dart';
import 'package:bus_track_app/screens/publicidad_screen.dart';
import 'package:bus_track_app/services/bus.service.dart';
import 'package:bus_track_app/services/service_locator.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_animarker/flutter_map_marker_animation.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import '../models/route.dart' as BusRoute;

// ignore: must_be_immutable
class MapScreen extends StatefulWidget {
  final BusLocation initialLocation;
  BusRoute.Route route;
  MapScreen({
    this.initialLocation =
        const BusLocation(latitude: 25.799678, longitude: -108.993624),
    this.route,
  });

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  LatLng _pickedLocation;
  bool banderaConeccion = false;
  //ignore: unused_field
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  final controller = Completer<GoogleMapController>();
  BitmapDescriptor _busIcon;
  BitmapDescriptor _personIcon;
  bool showMap = false;
  LatLng _userLocation;
  LatLng _lastMapPosition;
  //ignore: unused_field
  Bus _nextBus;
  Map<String, BusPosition> _busesData = {};
  final markers = <MarkerId, Marker>{};
  final Set<Polyline> _routePolyline = {};

  BusService _busService = servicerLocator<BusService>();
  Future<void> initConnectivity() async {
    ConnectivityResult result = ConnectivityResult.none;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  void _onCameraMove(CameraPosition position) {
    _lastMapPosition = position.target;
    print('valor de lastmapposition en oncameraMove');
    print(_lastMapPosition);
    print('valor de lastmapposition en oncameraMove');
    setState(() {
      MarkerId markerId = MarkerId("UbicacionSeleccionada");
      this.markers[markerId] = new Marker(
        icon: BitmapDescriptor.defaultMarker,
        infoWindow:
            InfoWindow(title: 'Lugar de destino', snippet: 'Lugar de destino'),
        markerId: markerId,
        position: _lastMapPosition,
      );
    });
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        print('fuewifi');
        if (banderaConeccion == true) {
          setState(() {
            banderaConeccion = false;
          });
          Navigator.pop(context);
        }
        break;

      case ConnectivityResult.mobile:
        print('fuemobil');
        if (banderaConeccion == true) {
          setState(() {
            banderaConeccion = false;
          });
          Navigator.pop(context);
        }
        break;

      case ConnectivityResult.none:
        setState(() => _connectionStatus = result.toString());
        setState(() {
          banderaConeccion = true;
        });
        // if (banderaConeccion) {
        //   Navigator.pop(context);
        // }

        return showDialog(
          barrierDismissible: false,
          context: context,
          barrierColor: Colors.black.withOpacity(0.8),
          builder: (context) {
            return StatefulBuilder(
              builder: (context, setState) {
                // willpopScope si da return false, el usuario no puede cerrarlo por su propia cuenta el alertdialog
                return WillPopScope(
                  onWillPop: () async {
                    return false;
                  },
                  child: AlertDialog(
                    title: Text(
                      'Reconectando...',
                      style: TextStyle(fontSize: 18),
                    ),
                    content: Container(
                      height: 130,
                      decoration: BoxDecoration(),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                              'Algo salio mal al intentar cargar los datos, por favor verifique su conexion a internet.'),
                          SizedBox(
                            height: 15,
                          ),
                          SizedBox(
                            height: 55,
                            width: 45,
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          ),
                        ],
                      ),
                    ),
                    actions: [
                      // ignore: deprecated_member_use
                      FlatButton(
                        child: Text('Salir de la aplicacion'),
                        onPressed: () {
                          SystemNavigator.pop();
                        },
                      ),
                    ],
                  ),
                );
              },
            );
          },
        );
        break;

      default:
        setState(() => _connectionStatus = 'Failed to get connectivity.');
        break;
    }
  }

  @override
  void initState() {
    initializeIcons();
    if (tiempoDiferencia > 5) {
      tiempoDiferencia = 0;
      showPublicity();
    }
    WidgetsBinding.instance.addPostFrameCallback((_) {
      initConnectivity();
      _connectivitySubscription =
          Connectivity().onConnectivityChanged.listen(_updateConnectionStatus);
    });
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    print('valor de cancel ');
    print(_connectivitySubscription);
    super.dispose();
  }

  // obtiene la ubicacion actual del usuario
  Future<void> getCurrentUserLocation() async {
    var userLocation = await Location().getLocation();

    setState(() {
      this._userLocation =
          LatLng(userLocation.latitude, userLocation.longitude);
      this.showMap = true;
      MarkerId markerId = MarkerId("userLocation");
      this.markers[markerId] = new Marker(
        icon: _personIcon,
        infoWindow:
            InfoWindow(title: 'Tu localizacion', snippet: 'Este eres Tu'),
        markerId: markerId,
        position: _userLocation,
      );
    });
  }

  printRoutePolyline() {
    _routePolyline.add(Polyline(
      polylineId: PolylineId('routePolyline'),
      visible: true,
      points: widget.route.wayPoints,
      width: 3,
      color: Color.fromRGBO(0, 100, 255, 0.5),
    ));
  }

  showPublicity() {
    Timer(
        Duration(seconds: 5),
        () => Navigator.push(context,
            MaterialPageRoute(builder: (context) => PublicidadScreen())));
  }

  listenForRouteBuses() {
    var socket = SocketHelper().getSocket();
    var bodySocket = {
      'routeId': widget.route.id,
    };
    socket.emit('subscribeToRoute', bodySocket);

    socket.on('route', (valueJson) {
      newBusLocationUpdate(BusPosition.fromJson(valueJson));
    });
  }

  initializeIcons() async {
    var busIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2), "assets/images/bus.png");
    var personIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 100),
        "assets/images/iconPerson.png");

    setState(() {
      this._busIcon = busIcon;
      this._personIcon = personIcon;
    });
  }

  initializeRouteBusesData() async {
    List<Bus> busesList = await _busService.getBusesByRoute(widget.route.id);
    busesList.forEach((b) {
      if (_busesData[b.id] == null) _busesData[b.id] = BusPosition.fromBus(b);
    });
  }

  void _selectLocation(LatLng position) {
    setState(() {
      MarkerId markerId = MarkerId("selectedLocation");
      this.markers[markerId] = new Marker(
        // icon: _personIcon,
        infoWindow: InfoWindow(
            title: 'localizacion seleccionada', snippet: 'Lugar de destino'),
        markerId: markerId,
        position: position,
      );
      _pickedLocation = position;
      print('valor de _pickedLocation');
      print(_pickedLocation);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: isSelecting == true
          ? AppBar(
              title: Text('Selecciona la ubicacion en el mapa'),
              actions: [
                IconButton(
                  onPressed: () {
                    setState(() {
                      isSelecting = false;
                      print('XXX valor de camera position = ');
                      print(CameraPosition);
                      print('valor de camera position XXX');
                      // aqui va el metodo para comparar las rutas
                      print('valor de isselecting');
                      print(isSelecting);
                    });
                  },
                  icon: Icon(Icons.check),
                ),
                IconButton(
                  onPressed: () {
                    setState(() {
                      isSelecting = false;
                      Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (context) => AfterSplash()),
                        (Route<dynamic> route) => false,
                      );
                    });
                  },
                  icon: Icon(Icons.cancel_outlined),
                ),
              ],
              backgroundColor: vinoTinto,
            )
          : null,
      body: Animarker(
        useRotation: false,
        duration: isSelecting == true ? Duration(milliseconds: 20) : Duration(milliseconds: 2000),
        mapId: controller.future
            .then<int>((value) => value.mapId), //Grab Google Map Id
        markers: markers.values.toSet(),
        child: GoogleMap(
          onCameraMove: _onCameraMove,
          zoomControlsEnabled: false,
          mapToolbarEnabled: false,
          mapType: MapType.normal,
          polylines: _routePolyline,
          initialCameraPosition:
              CameraPosition(target: Config.INITIAL_CAMERA_POSITION, zoom: 14),
          onMapCreated: (gController) {
            getCurrentUserLocation();
            if (widget.route != null) {
              initializeRouteBusesData();
              printRoutePolyline();
              listenForRouteBuses();
            }
            controller.complete(gController);
          }, //Complete the future GoogleMapController
          onTap: isSelecting == true ? _selectLocation : null,
        ),
      ),
    );
  }

  void newBusLocationUpdate(BusPosition busData) async {
    var newMarker = getBusMarker(busData);
    setState(() => markers[newMarker.markerId] = newMarker);
    _busesData[busData.busId].updateData(busData);
    checkForNextBus();
  }

  Marker getBusMarker(BusPosition busData) {
    Marker busMarker = Marker(
      markerId: MarkerId(busData.busId),
      icon: _busIcon,
      position: busData.position,
    );
    return busMarker;
  }

  void checkForNextBus() {
    var nearestPoints = MapsHelper.getNearestPointPreviousAndNext(
        _userLocation, widget.route.wayPoints);
    var nextBusId =
        MapsHelper.getNextBus(nearestPoints, _busesData.values.toList());
    print(nextBusId);
  }
}
