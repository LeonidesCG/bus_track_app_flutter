import 'dart:convert';
import 'package:bus_track_app/helpers/config.dart';
import 'package:bus_track_app/models/globals.dart';
import 'package:bus_track_app/models/publicidad.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import '../models/route.dart' as BusRoute;

import '../main.dart';

// ignore: must_be_immutable
class PublicidadScreen extends StatefulWidget {
  BusRoute.Route route;
  PublicidadScreen({this.route});
  @override
  _PublicidadScreenState createState() => _PublicidadScreenState();
}

class _PublicidadScreenState extends State<PublicidadScreen> {
  // List<Publicidad> publicidad = [];
  var publicidad;
  var imgUrl = apiUrl + 'public/publicity/';
  @override
  void initState() {
    super.initState();

    if (horaDeUltimaPublicidad == null || tiempoDiferencia > 5) {
      horaDeUltimaPublicidad = new DateTime.now();
      tiempoDiferencia = 0;
    }
    if (horaDeUltimaPublicidad != null) {
      tiempoDiferencia =
          DateTime.now().difference(horaDeUltimaPublicidad).inMinutes;
    }
    fetchPostPublicidad();
  }

  void fetchPostPublicidad() async {
    // if (widget.route != null) {
    //   if (widget.route.id != null) {
    //     final response = await http.get(
    //       Uri.parse(Config.API_URL + 'publicity?routeId=' + widget.route.id),
    //       headers: {'Content-Type': 'application/json'},
    //     );
    //     if (response.statusCode == 200) {
    //       parsePostPublicidad(response.body);
    //     } else {
    //       final response =
    //           await http.get(Uri.parse(Config.API_URL + 'publicity'));
    //       if (response.statusCode == 200 && response.body != []) {
    //         parsePostPublicidad(response.body);
    //       } else {
    //         throw Exception("Request API Error");
    //       }
    //     }
    //   }
    // } else {
      final response = await http.get(Uri.parse(Config.API_URL + 'publicity'));
      if (response.statusCode == 200) {
        parsePostPublicidad(response.body);
      } else {
        throw Exception("Request API Error");
      }
    // }
  }

  void parsePostPublicidad(String responseBody) {
    List<dynamic> rawData = json.decode(responseBody);
    publicidad = [];
    rawData.forEach((p) {
      setState(() {
        publicidad.add(Publicidad.fromJson(p));
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [Colors.transparent, Colors.black]),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          child: Center(
            child: ListView(
              children: [
                Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 5, 15, 5),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      width: 25.0,
                      height: 25.0,
                      child: new RawMaterialButton(
                        shape: new CircleBorder(),
                        elevation: 0.0,
                        child: Icon(Icons.close),
                        onPressed: () {
                          tiempoDiferencia = 0;
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(
                              builder: (context) => AfterSplash(
                                ruta: widget.route,
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ),
                if (publicidad != null &&
                    publicidad[0] != null &&
                    publicidad[0].imgName != null)
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(0)),
                      child: new Image(
                        image: new NetworkImage(
                          '$imgUrl${publicidad[0].imgName}',
                        ),
                        height: 400,
                        width: double.infinity,
                        fit: BoxFit.fill,
                      ),
                    ),
                  )
                else
                  SizedBox(
                    height: 0,
                    width: 0,
                  ),
                if (publicidad != null &&
                    publicidad[0] != null &&
                    publicidad[0].title != '')
                  Padding(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 5),
                      child: Text(
                        publicidad[0].title,
                        style: GoogleFonts.openSans(
                          textStyle: TextStyle(
                            fontFamily: 'Open Sans',
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ))
                else
                  SizedBox(
                    height: 30,
                    width: 30,
                  ),
                if (publicidad != null &&
                    publicidad[0] != null &&
                    publicidad[0].description != '')
                  Padding(
                    padding: const EdgeInsets.fromLTRB(25, 10, 17, 10),
                    child: Text(
                      publicidad[0].description,
                      style: GoogleFonts.openSans(
                        textStyle: TextStyle(
                            fontFamily: 'Open Sans',
                            fontSize: 16,
                            color: Colors.white),
                      ),
                    ),
                  )
                else
                  SizedBox(
                    height: 40,
                    width: 30,
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
