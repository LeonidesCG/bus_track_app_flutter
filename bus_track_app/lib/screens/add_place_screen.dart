import 'dart:convert';
import 'dart:io';
import 'package:bus_track_app/constants/colors.dart';
import 'package:bus_track_app/helpers/config.dart';
import 'package:bus_track_app/models/busData.dart';
import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

import '../widgets/image_input.dart';
import 'package:http/http.dart' as http;

class AddPlaceScreen extends StatefulWidget {
  static const routeName = '/add-place';

  @override
  _AddPlaceScreenState createState() => _AddPlaceScreenState();
}

class _AddPlaceScreenState extends State<AddPlaceScreen> {
  final _descriptionController = TextEditingController();
  final _phoneController = TextEditingController();

  // ignore: unused_field
  File _pickedImage;

  String identifier = "";
  String busChoose;
  var url = apiUrl;
  List<BusData> busList = [];

  void parsePostBusData(String responseBody) {
    var list = json.decode(responseBody) as List<dynamic>;
    var posts = list.map((model) => BusData.fromJson(model)).toList();
    setState(() {
      busList = posts;
      busList
          .sort((a, b) => int.parse(a.number).compareTo(int.parse(b.number)));
    });
    // return posts;
  }

  void fetchPostBusData() async {
    final response = await http.get(Uri.parse(Config.API_URL + 'bus'));

    if (response.statusCode == 200) {
      parsePostBusData(response.body);
      // return compute(parsePostBusData, response.body);
    } else {
      throw Exception("Request API Error");
    }
  }

  void _selectImage(File pickedImage) {
    _pickedImage = pickedImage;
  }

  Future<void> _getDeviceDetails() async {
    final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        var build = await deviceInfoPlugin.androidInfo;
        setState(() {
          identifier = build.androidId;
        });
      } else if (Platform.isIOS) {
        var build = await deviceInfoPlugin.iosInfo;
        setState(() {
          identifier = build.identifierForVendor;
        });
      }
    } catch (error) {
      print(error);
      return showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text('Ocurrio un error!'),
          content: Text(
              'Algo salio mal al intentar obtener el id de su telefono, por favor, asegúrese que acepto los permisos necesarios.'),
          actions: [
            // ignore: deprecated_member_use
            FlatButton(
                child: Text('Continuar'),
                onPressed: () {
                  returnPage();
                })
          ],
        ),
      );
    }
  }

  _checkForm() {
    if (_descriptionController.value.text == '' ||
        busChoose == null ||
        identifier == null ||
        _pickedImage == null) {
      return showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text('Ocurrio un error!'),
          content: SizedBox(
            height: 100,
            child: Column(
              children: [
                Text(
                  'Algo salio mal al intentar enviar su reporte.\nPor favor, asegúrese que lleno los datos correctamente',
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          actions: [
            // ignore: deprecated_member_use
            RaisedButton(
              child: Text('Continuar'),
              onPressed: () {
                returnPage();
              },
            ),
          ],
        ),
      );
    }
  }

  returnPage() {
    Navigator.of(context).pop();

    // Navigator.of(context).pushReplacement(
    //   MaterialPageRoute(
    //     builder: (context) => PlacesListScreen(),
    //   ),
    // );
  }

  Future _sendReport() async {
    _checkForm();

    try {
      var file = MultipartFile.fromBytes(
        await _pickedImage.readAsBytes(),
        filename: identifier,
      );
      var body = new FormData.fromMap({
        'title': '',
        'bus': busChoose,
        "phoneId": identifier,
        "description": _descriptionController.value.text,
        "contactPhoneNumber": _phoneController.value.text,
        "img": file
      });

      var dio = Dio();
      // ignore: unused_local_variable
      var response = await dio.post(url + 'report', data: body);

      // navegar hacia la nueva pestana donde estan mis reportes
      Flushbar(
        title: 'Reporte',
        message:
            'Tu reporte ha sido enviado con exito. Te contactaremos en los proximos dias en la seccion de reportes de la app o al numero ' +
                _phoneController.value.text,
        duration: Duration(seconds: 5),
        flushbarPosition: FlushbarPosition.BOTTOM,
        flushbarStyle: FlushbarStyle.GROUNDED,
        reverseAnimationCurve: Curves.decelerate,
        forwardAnimationCurve: Curves.slowMiddle,
        backgroundColor: Colors.grey,
        boxShadows: [
          BoxShadow(
            color: vinoTinto,
            offset: Offset(0.0, 2.0),
            blurRadius: 3.0,
          ),
        ],
      ).show(context).then(
            (value) => Navigator.of(context).pop(),
          );
    } catch (err) {
      print(err);
    }
  }

  @override
  void initState() {
    _getDeviceDetails();
    fetchPostBusData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Hacer un reporte'),
        backgroundColor: vinoTinto,
      ),
      body: Container(
        decoration: BoxDecoration(
          color: Color(0xFFffffff),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 10),
                      TextField(
                        // keyboardType: TextInputType.text,
                        keyboardType: TextInputType.multiline,
                        minLines: 9,
                        maxLines: null,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelStyle: TextStyle(color: darkGrey),
                            labelText: 'Detalles del reporte:',
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                              borderRadius: BorderRadius.all(
                                Radius.circular(2),
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: vinoTinto),
                              borderRadius: BorderRadius.all(
                                Radius.circular(2),
                              ),
                            ),
                            prefixIcon: Icon(
                              Icons.report,
                              color: Colors.grey,
                            )),
                        controller: _descriptionController,
                      ),
                      SizedBox(height: 10),
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: Container(
                            padding: EdgeInsets.only(left: 16, right: 16),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey, width: 1),
                              borderRadius: BorderRadius.circular(2),
                            ),
                            child: SizedBox(
                              height: 55,
                              child: DropdownButton(
                                hint: busList != null && busList.length > 0
                                    ? Text(
                                        'Seleccione el autobus donde ocurrio el percante')
                                    : Text('No hay autobuses disponibles'),
                                icon: Icon(Icons.arrow_drop_down),
                                dropdownColor: Colors.grey[50],
                                iconSize: 36,
                                isExpanded: true,
                                underline: SizedBox(),
                                value: busChoose,
                                onChanged: (newValue) {
                                  setState(() {
                                    busChoose = newValue;
                                  });
                                },
                                items: busList.map((b) {
                                  return DropdownMenuItem(
                                    value: b.id,
                                    child: Text(
                                      "Autobus #" + b.number.toString(),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      TextField(
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelStyle: TextStyle(color: darkGrey),
                            labelText: 'Numero de telefono de contacto:',
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                              borderRadius: BorderRadius.all(
                                Radius.circular(5),
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: vinoTinto),
                              borderRadius: BorderRadius.all(
                                Radius.circular(5),
                              ),
                            ),
                            prefixIcon: Icon(
                              Icons.phone,
                              color: Colors.grey,
                            )),
                        controller: _phoneController,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      ImageInput(_selectImage),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            // ignore: deprecated_member_use
            Container(
              padding: EdgeInsets.fromLTRB(10, 5, 10, 0),
              // ignore: deprecated_member_use
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Text(
                  'Enviar Reporte',
                  style: GoogleFonts.openSans(
                    textStyle: TextStyle(
                      fontFamily: 'Open Sans',
                      fontSize: 15,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                onPressed: () {
                  _sendReport();
                },
                elevation: 5,
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                color: Color(0xFFcc0000),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              // ignore: deprecated_member_use
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Text(
                  'Cancelar',
                  style: GoogleFonts.openSans(
                    textStyle: TextStyle(
                        fontFamily: 'Open Sans',
                        fontSize: 15,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                onPressed: returnPage,
                elevation: 5,
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                color: Color(0xFFcc0000),
              ),
            ),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}
