import 'dart:convert';
import 'package:bus_track_app/constants/colors.dart';
import 'package:bus_track_app/helpers/config.dart';
import 'package:bus_track_app/models/reports.dart';
import 'package:bus_track_app/screens/display_report_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import './add_place_screen.dart';

class PlacesListScreen extends StatefulWidget {
  static const routeName = '/place-list';

  @override
  _PlacesListScreenState createState() => _PlacesListScreenState();
}

class _PlacesListScreenState extends State<PlacesListScreen> {
  var imgUrl = apiUrl + 'public/reports/';
  var imgName;
  String deviceId = "961a12bbe10b1045";

  todayDate(e) {
    DateTime dt = DateTime.parse(e);
    var formatter = new DateFormat('dd-MM-yyyy');
    String formattedTime = DateFormat('hh:mm:a').format(dt);
    String formattedDate = formatter.format(dt);
    return formattedDate + '  ' + formattedTime;
  }

  List<Report> reportList = [];

  var contenedor = new Padding(
    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
    child: ClipOval(
      child: new Image(
        image: AssetImage("assets/images/imagenNoDisponible.png"),
        height: 70,
        width: 70,
        fit: BoxFit.cover,
      ),
    ),
  );

  void parsePostReports(String responseBody) {
    var list = json.decode(responseBody) as List<dynamic>;
    var posts = list.map((model) => Report.fromJson(model)).toList();
    setState(() {
      reportList = posts;
    });
  }

  void fetchPostReports() async {
    var reqBody = {"phoneId": deviceId};
    final response = await http.post(
      Uri.parse(Config.API_URL + 'report/find'),
      body: JsonEncoder().convert(reqBody),
      headers: {'Content-Type': 'application/json'},
    );

    if (response.statusCode == 200) {
      parsePostReports(response.body);
      // return compute(parsePostBusData, response.body);
    } else {
      throw Exception("Request API Error");
    }
  }

  @override
  @override
  void initState() {
    super.initState();
    cargoDeReports();
  }

  cargoDeReports() {
    fetchPostReports();
  }

  returnPage() {
    Navigator.of(context).pop();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Tus Reportes',
          style: GoogleFonts.openSans(
            textStyle: TextStyle(
              fontFamily: 'Open Sans',
              fontSize: bodyFont,
            ),
          ),
        ),
        backgroundColor: vinoTinto,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              Navigator.of(context).pushNamed(AddPlaceScreen.routeName);
            },
          ),
        ],
      ),
      body: reportList.length <= 0
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Aun no has echo ningun reporte',
                    style: GoogleFonts.openSans(
                      textStyle: TextStyle(
                        fontFamily: 'Open Sans',
                        fontSize: bodyFont,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: new Container(
                            margin:
                                const EdgeInsets.only(left: 0.0, right: 5.0),
                            child: Divider(
                              color: Colors.black,
                              height: 25,
                            )),
                      ),
                      Text(
                        'Crear Reporte',
                        style: GoogleFonts.openSans(
                          textStyle: TextStyle(
                            fontFamily: 'Open Sans',
                            fontSize: bodyFont,
                          ),
                        ),
                      ),
                      Expanded(
                        child: new Container(
                          margin: const EdgeInsets.only(left: 5.0, right: 0.0),
                          child: Divider(
                            color: Colors.black,
                            height: 25,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  FloatingActionButton(
                    onPressed: () {
                      // Add your onPressed code here!
                      Navigator.of(context).pushNamed(AddPlaceScreen.routeName);
                    },
                    child: const Icon(Icons.post_add_rounded),
                    backgroundColor: vinoTinto,
                  ),
                ],
              ),
            )
          : Scaffold(
              backgroundColor: lightGray,
              body: ListView.builder(
                itemCount: reportList.length,
                padding: new EdgeInsets.symmetric(vertical: 16.0),
                itemBuilder: (ctx, i) => GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => DisplayReportScreen(
                          data: reportList[i],
                        ),
                      ),
                    );
                  },
                  child: new Stack(
                    children: [
                      // planetCardContent
                      Container(
                        child: Container(
                          margin: new EdgeInsets.fromLTRB(65.0, 8.0, 16.0, 8.0),
                          constraints: new BoxConstraints.expand(),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                todayDate(reportList[i].date),
                                style: GoogleFonts.openSans(
                                  textStyle: TextStyle(
                                    fontFamily: 'Open Sans',
                                    fontSize: font15,
                                  ),
                                ),
                              ),
                              new Text(
                                reportList[i].description.length > 40
                                    ? reportList[i]
                                            .description
                                            .substring(0, 28) +
                                        '...'
                                    : reportList[i].description,
                                style: GoogleFonts.openSans(
                                  textStyle: TextStyle(
                                    fontFamily: 'Open Sans',
                                    fontSize: font12,
                                    color: mediumGray,
                                  ),
                                ),
                              ),
                              new Container(
                                margin: new EdgeInsets.symmetric(
                                    vertical: 8.0, horizontal: 200),
                                child: Icon(
                                  Icons.question_answer,
                                  color: reportList[i].response != null
                                      ? verde
                                      : mediumGray,
                                ),
                              )
                            ],
                          ),
                        ),
                        height: 92.0,
                        margin:
                            new EdgeInsets.only(left: 30.0, right: 10, top: 10),
                        decoration: new BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.rectangle,
                          borderRadius: new BorderRadius.circular(8.0),
                          boxShadow: <BoxShadow>[
                            new BoxShadow(
                              color: Colors.black12,
                              blurRadius: 10.0,
                              offset: new Offset(0.0, 10.0),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0, top: 20),
                        child: reportList[i].imgName != null
                            ? ClipOval(
                                child: new Image(
                                  image: new NetworkImage(
                                    '$imgUrl${reportList[i].imgName}',
                                  ),
                                  height: 70,
                                  width: 70,
                                  fit: BoxFit.cover,
                                ),
                              )
                            : contenedor,
                      ),
                    ],
                  ),
                ),
              ),
            ),
      floatingActionButton: reportList.length > 0
          ? FloatingActionButton(
              onPressed: () {
                // Add your onPressed code here!
                Navigator.of(context).pushNamed(AddPlaceScreen.routeName);
              },
              child: const Icon(Icons.post_add_rounded),
              backgroundColor: vinoTinto,
            )
          : null,
    );
  }
}
