import 'package:flutter/material.dart';

Color primaryTextColor = Color(0xFF000000);
Color secondaryTextColor = Color(0xFFffffff);
Color titleTextColor = Colors.white;
Color lightGray = Color(0xFf2f2f2);
Color mediumGray = Color(0xFFbfbfbf);
Color darkGrey = Colors.grey[800];
Color vinoTinto = Color(0xFF800000);
Color rojoFuerte = Color(0xFFff0000);
Color verde = Color(0xFF2ecc71);

