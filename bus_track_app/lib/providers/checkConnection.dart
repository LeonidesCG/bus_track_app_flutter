import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// import 'package:flutter/cupertino.dart';

// class CheckConnection with ChangeNotifier {

connectionCheck(BuildContext context) async {
  var revisando = false;
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      print('Se conecto a internet');
    }
  } on SocketException catch (_) {
    print('no esta conectado');
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              title: revisando == true
                  ? Text('Reconectando')
                  : Text('Ocurrio un error!'),
              content: revisando == true
                  ? SizedBox(
                      height: 55,
                      width: 20,
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    )
                  : Text(
                      'Algo salio mal al intentar cargar los datos, por favor verifique su conexion a internet.'),
              actions: [
                // ignore: deprecated_member_use
                FlatButton(
                    child: Text('Intentar de nuevo'),
                    onPressed: () async {
                      setState(() {
                        revisando = true;
                        print('revisando = ' + revisando.toString());
                      });
                      try {
                        final result =
                            await InternetAddress.lookup('google.com');
                        if (result.isNotEmpty &&
                            result[0].rawAddress.isNotEmpty) {
                          print('Se conecto a internet');
                          Timer(
                            Duration(seconds: 1),
                            () => setState(() {
                              revisando = false;
                            }),
                          );
                          Timer(
                            Duration(seconds: 2),
                            () => Navigator.pop(context),
                          );
                        } else {
                          final result =
                              await InternetAddress.lookup('google.com');
                          if (result.isNotEmpty &&
                              result[0].rawAddress.isNotEmpty) {
                            print('Se conecto a internet');
                            Timer(
                              Duration(seconds: 1),
                              () => setState(() {
                                revisando = false;
                              }),
                            );
                            Timer(
                              Duration(seconds: 2),
                              () => Navigator.pop(context),
                            );
                          }
                        }
                      } on SocketException catch (_) {
                        Timer(
                          Duration(seconds: 1),
                          () => setState(() {
                            revisando = false;
                          }),
                        );
                      }
                    }),
                // ignore: deprecated_member_use
                FlatButton(
                    child: Text('Salir'),
                    onPressed: () {
                      SystemNavigator.pop();
                    }),
              ],
            );
          },
        );
      },
    );
  }
}
