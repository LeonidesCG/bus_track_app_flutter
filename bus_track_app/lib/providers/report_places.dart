
import 'package:bus_track_app/models/bus.dart';
import 'package:bus_track_app/models/reports.dart';
import 'package:flutter/foundation.dart';


class ReportPlaces with ChangeNotifier {
  List<Report> _items = [];

  List<Report> get items {
    return [..._items];
  }

  void addPlace(
    String pickedTitle,
    Bus pickedBus,
    String pickedDescription,
    String pickedPhoneId,
    String pickedImage,
  ) {
    final newReport = Report(
      title: pickedTitle,
      bus: pickedBus,
      description: pickedDescription,
      phoneId: pickedPhoneId,
      imgName: pickedImage,
    );
    _items.add(newReport);
    notifyListeners();
  }
}
