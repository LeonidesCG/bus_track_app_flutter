

class Publicidad {
  bool active;
  String title;
  String description;
  String imgName;
  Publicidad({
    this.active,
    this.title,
    this.description,
    this.imgName,
  });

  Publicidad.fromJson(Map<String, dynamic> json) {
    active = json['active'];
    title = json['title'];
    description = json['description'];
    imgName = json['imgName'];
  }
}

