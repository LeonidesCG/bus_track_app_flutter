import 'package:bus_track_app/models/route.dart';

class BusData {
  String id;
  String name;
  String number;
  Route route;
  String device;

  BusData({
    this.id,
    this.name,
    this.number,
    this.route,
    this.device,
  });

  BusData.fromJson(Map<String, dynamic> json) {
    try {
      id = json['_id'];
      name = json['name'];
      number = json['number'];
      route = json['route'];
      device = json['device'];
    } catch (err) {
      print(err);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.id;
    data['name'] = this.name;
    data['number'] = this.number;
    data['route'] = this.route;
    data['device'] = this.device;
    return data;
  }
}
