import 'package:bus_track_app/helpers/maps_helper.dart';
import 'package:bus_track_app/models/route.dart' as BusRoute;
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ParsedRoute {
  List<RouteWayPoint> wayPoints;

  LatLng nearestPoint;
  LatLng prevWayPoint;
  LatLng nextWayPoint;
  List<LatLng> middleWayPoints;

  ParsedRoute.parseFromRoute(BusRoute.Route route) {
    if (route?.wayPoints == null) return;

    this.wayPoints = [];
    for (var i = 0; i < route.wayPoints.length; i++) {
      this.wayPoints.add(new RouteWayPoint(
            route.wayPoints[i],
            route.wayPoints[(i < route.wayPoints.length - 1) ? i + 1 : 0],
          ));
    }
  }
}

class NearestPoint {
  LatLng latLng;
  LatLng previous;
  LatLng next;
  double angle;
  List<LatLng> middleWayPoints;
}

class RouteWayPoint {
  LatLng latLng;
  double angleToNextPoint;
  List<LatLng> semiWayPoints;
  RouteWayPoint(LatLng wayPoint, LatLng nextWayPoint) {
    this.latLng = wayPoint;
    this.angleToNextPoint = MapsHelper.getAngle(wayPoint, nextWayPoint);
    this.semiWayPoints =
        MapsHelper.getWayPointsFromAToBByDistance(wayPoint, nextWayPoint);
  }
}
