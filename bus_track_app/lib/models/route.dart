import 'package:google_maps_flutter/google_maps_flutter.dart';

class Route {
  bool active;
  List<LatLng> wayPoints;
  int routeLength;
  String id;
  String name;
  List<dynamic> places;

  Route({this.active, this.wayPoints,this.routeLength, this.id, this.name, this.places});

  Route.fromJson(Map<String, dynamic> json) {
    try {
      active = json['active'];
      places = json['places'];
      routeLength = json['routeLength'];
      id = json['_id'];
      name = json['name'];
      wayPoints = [];
      // wayPoints = (json['wayPoints'] as List<dynamic>).map((wp) => LatLng(wp["lat"], wp["lng"]));
      
      if(json["wayPoints"] == null)
        return;
      var _wayPoints = json["wayPoints"];
      _wayPoints.forEach((wp) {
        wayPoints.add(LatLng(wp["lat"], wp["lng"]));
      });
      // print(_wayPoints);
    } catch(err) {
      print(err);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    
    data['active'] = this.active;
    data['places'] = this.places;
    data['routeLength'] = this.routeLength;
    data['_id'] = this.id;
    data['name'] = this.name;
    data['wayPoints'] = this.wayPoints;
    return data;
  }

}