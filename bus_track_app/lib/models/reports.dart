import 'package:bus_track_app/models/route.dart';
import 'route.dart';
import 'bus.dart';

class Report {
  String title;
  Bus bus;
  String description;
  String phoneId;
  String imgName;
  String date;
  Route route;
  ReportResponse response;
  Report({
    this.title,
    this.bus,
    this.description,
    this.phoneId,
    this.imgName,
    this.date,
    this.route,
  });

  Report.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    description = json['description'];
    phoneId = json['phoneId'];
    imgName = json['imgName'];
    date = json['date'];

    if (json['bus'] != null) {
      bus = Bus.fromJson(json['bus']);
    }
    
    if(json['bus'] != null && json['bus'] is Map<String, dynamic>)
      bus = Bus.fromJson(json['bus']);
    else if(json['route'] != null && json['bus'] is String) {
      bus = Bus(id: json['bus']);
    }

    if(json['route'] != null && json['route'] is Map<String, dynamic>)
      route = Route.fromJson(json['route']);
    else if(json['route'] != null && json['route'] is String) {
      route = Route(id: json['route']);
    }

    if (json['response'] != null) {
      response = ReportResponse.fromJson(json['response']);
    }
  }
}

class ReportResponse {
  String text;
  DateTime date;
  ReportResponse({
    this.text,
    this.date,
  });
  ReportResponse.fromJson(Map<String, dynamic> json) {
    text = json['text'];
    date = DateTime.parse(json['date']);
  }
}

// {
//     "title": "Chofer Grosero",
//     "bus": "60b2ea095fe32a646cab12bd",
//     "description": "El chofer se porto muy grosero cuando se subio una señora mayor y cuando le reclame me dijo que mejor me callara",
//     "phoneId": "123456",
//     "imgName": null,
//     "response": null
// }
