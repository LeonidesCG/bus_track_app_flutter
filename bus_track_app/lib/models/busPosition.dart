import 'package:bus_track_app/models/bus.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class BusPosition {
  String busId;
  String routeId;
  String deviceId;
  LatLng position;
  LatLng previousPosition;
  double speed;
  DateTime date;
  Bus bus;

  BusPosition(
      {this.busId,
      this.routeId,
      this.deviceId,
      this.position,
      this.speed,
      this.date});

  BusPosition.fromJson(Map<String, dynamic> json) {
    try {
      busId = json["bus"];
      routeId = json["route"];
      deviceId = json["deviceId"];
      position = LatLng(double.parse(json["lat"].toString()),
          double.parse(json["lng"].toString()));
      speed = double.parse(json["speed"].toString());
      date = DateTime.parse(json["date"].toString());
    } catch (err) {
      print(err);
    }
  }

  BusPosition.fromBus(Bus bus) {
    this.bus = bus;
    this.busId = bus.id;
    this.routeId = bus.route?.id;
    this.deviceId = bus.device?.id;
  }

  updateData(BusPosition newData) {
    this.previousPosition = this.position;
    this.position = newData.position;
    this.speed = newData.speed;
    this.date = newData.date;
  }
}
