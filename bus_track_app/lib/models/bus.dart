

import 'package:flutter/foundation.dart';
import 'route.dart';
import 'device.dart';

class BusLocation {
  final double latitude;
  final double longitude;

  const BusLocation({
    @required this.latitude,
    @required this.longitude,
  });
}

class Bus {
  String id;
  String name;
  String number;
  Route route;
  Device device;
  BusLocation location;

  Bus({
    this.id,
    this.name,
    this.number,
    this.route,
    this.device,
    this.location,
  });

  Bus.fromJson(Map<String, dynamic> json) {
    id = json['_id'];
    name = json['name'];
    number = json['number'];

    if (json['route'] != null && json['route'] is Map<String, dynamic>)
      route = Route.fromJson(json['route']);
    else if (json['route'] != null && json['route'] is String) {
      route = Route();
      route.id = json['route'];
    }

    if (json['device'] != null && json['device'] is Map<String, dynamic>)
      device = Device.fromJson(json['device']);
    else if (json['route'] != null && json['route'] is String) {
      device = Device();
      device.id = json['device'];
    }
  }
}
