import 'dart:math' as Math;
import 'package:bus_track_app/models/bus.dart';
import 'package:bus_track_app/models/busPosition.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapsHelper {
  static double calculateDistance(LatLng a, LatLng b) {
    var R = 6371; // km
    var dLat = _toRad(b.latitude - a.latitude);
    var dLon = _toRad(b.longitude - a.longitude);
    var lat1 = _toRad(a.latitude);
    var lat2 = _toRad(b.latitude);

    var x = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.sin(dLon / 2) *
            Math.sin(dLon / 2) *
            Math.cos(lat1) *
            Math.cos(lat2);

    var c = 2 * Math.atan2(Math.sqrt(x), Math.sqrt(1 - x));
    var d = R * c;
    return d;
  }

  static LatLng getNearestPoint(LatLng a, List<LatLng> routeWayPoints) {
    LatLng nearestPoint;
    double nearestDistance = 999999;
    for (var i = 0; i < routeWayPoints.length; i++) {
      List<LatLng> virtualWayPoints = getWayPointsFromAToBByDistance(
          routeWayPoints[i],
          (i == routeWayPoints.length - 1)
              ? routeWayPoints[0]
              : routeWayPoints[i + 1]);
      for (var j = 0; j < virtualWayPoints.length; j++) {
        double newDistance = calculateDistance(a, virtualWayPoints[j]);
        if (newDistance < nearestDistance) {
          nearestDistance = newDistance;
          nearestPoint = virtualWayPoints[j];
        }
      }
    }

    return nearestPoint;
  }

  static Map<String, LatLng> getNearestPointPreviousAndNext(
      LatLng a, List<LatLng> routeWayPoints) {
    LatLng nearestPoint;
    double nearestDistance = 999999;
    int foundIndex = 0;
    for (var i = 0; i < routeWayPoints.length; i++) {
      List<LatLng> virtualWayPoints = getWayPointsFromAToBByDistance(
          routeWayPoints[i],
          (i == routeWayPoints.length - 1)
              ? routeWayPoints[0]
              : routeWayPoints[i + 1]);
      for (var j = 0; j < virtualWayPoints.length; j++) {
        double newDistance = calculateDistance(a, virtualWayPoints[j]);
        if (newDistance < nearestDistance) {
          nearestDistance = newDistance;
          nearestPoint = virtualWayPoints[j];
          foundIndex = i;
        }
      }
    }

    return <String, LatLng>{
      'nearest': nearestPoint,
      'previous': routeWayPoints[foundIndex],
      'next': routeWayPoints[
          (foundIndex == routeWayPoints.length - 1 ? 0 : foundIndex + 1)]
    };
  }

  static List<LatLng> getWayPointsFromAToBByDistance(LatLng a, LatLng b) {
    double distance = calculateDistance(a, b);
    double latDistance = a.latitude - b.latitude;
    double lngDistance = a.longitude - b.longitude;
    int totalPoints = (distance * 1000 / 10).floor();

    List<LatLng> virtualWayPoints = [];

    virtualWayPoints.add(a);
    for (var i = 1; i <= totalPoints; i++) {
      virtualWayPoints.add(LatLng(
          a.latitude - ((latDistance / totalPoints) * i),
          a.longitude - ((lngDistance / totalPoints) * i)));
    }

    if (virtualWayPoints.length > 1) virtualWayPoints.removeLast();
    virtualWayPoints.add(b);
    return virtualWayPoints;
  }

  static List<Marker> getMarkersFromAToB(LatLng a, LatLng b) {
    List<Marker> markers = [];
    List<LatLng> wp = getWayPointsFromAToBByDistance(a, b);
    for (var i = 0; i < wp.length; i++) {
      markers.add(Marker(
          markerId: MarkerId('linePoint-' + i.toString()), position: wp[i]));
    }
    return markers;
  }

  static double _toRad(double val) {
    return val * Math.pi / 180;
  }

  static Bus getNextBus(
      Map<String, LatLng> wayPoints, List<BusPosition> busesData) {
    //ignore: unused_local_variable
    LatLng userPoint = wayPoints['nearest'];
    //ignore: unused_local_variable
    LatLng prevPoint = wayPoints['previous'];
    //ignore: unused_local_variable
    LatLng nextPoint = wayPoints['next'];

    return new Bus();
  }

  static double getAngle(LatLng a, LatLng b) {
    double lngDiff = b.longitude - a.longitude;
    double latDiff = b.latitude - a.latitude;
    double theta = Math.atan2(lngDiff, latDiff);
    theta *= 180 / Math.pi;
    if (theta < 0) theta = 360 + theta;

    return theta;
  }
  // function angle(cx, cy, ex, ey) {
  //   var dy = ey - cy;
  //   var dx = ex - cx;
  //   var theta = Math.atan2(dy, dx); // range (-PI, PI]
  //   theta *= 180 / Math.PI; // rads to degs, range (-180, 180]
  //   //if (theta < 0) theta = 360 + theta; // range [0, 360)
  //   return theta;
  // }
}
