import 'package:flutter/foundation.dart';
import 'package:bus_track_app/models/route.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'config.dart';

List<Route> parsePostReports(String responseBody) {
  var reportes = json.decode(responseBody);
  return reportes;
}

Future<List<Route>> fetchPostReports() async {
  final response = await http.get(Uri.parse(Config.API_URL + 'report'));
  if (response.statusCode == 200) {
    return compute(parsePostReports, response.body);
  } else {
    throw Exception("Request API Error");
  }
}
