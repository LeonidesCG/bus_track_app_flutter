import 'package:google_maps_flutter/google_maps_flutter.dart';

class Config {
  // static const String API_URL = 'http://192.168.100.5:3000/';
  static const String API_URL = 'http://35.222.47.117:3000/';
  
  // static const String API_URL = 'http://http://35.222.47.117/';
  static const LatLng INITIAL_CAMERA_POSITION = LatLng(25.790466, -108.985886);
  static const Map<String, String> HEADERS = <String, String>{
    'Content-Type': 'application/json; charset=UTF-8',
  };
}


var apiUrl = 'http://35.222.47.117:3000/';

// var apiUrl = 'http://192.168.100.20:3000/';
// var apiUrl = 'http://192.168.100.5:3000/';
// var apiUrl = 'http://192.168.100.6:3000/';

double titleFont = 16;
double bodyFont = 19;
double font12 = 12;
double font9 = 9;
double font15 = 15;
