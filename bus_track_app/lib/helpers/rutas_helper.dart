import 'package:flutter/foundation.dart';
import 'package:bus_track_app/models/route.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'config.dart';

List<Route> parsePost(String responseBody) {
  var list = json.decode(responseBody) as List<dynamic>;
  //List<Rutas> posts =  list.map((item) => Rutas.fromJson(item));
  List<Route> posts = [];
  list.forEach((item) {
    posts.add(Route.fromJson(item));
  });
  return posts;
}

Future<List<Route>> fetchPost() async {
  final response = await http.get(Uri.parse(Config.API_URL + 'route'));

  if (response.statusCode == 200) {
    return compute(parsePost, response.body);
  } else {
    throw Exception("Request API Error");
  }
}
