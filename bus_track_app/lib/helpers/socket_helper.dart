import 'package:flutter/cupertino.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

import 'config.dart';

class SocketHelper with ChangeNotifier {
  IO.Socket socket;
  var id;
  bool connectionChecker = false;
  var dataList;

  getSocket() {
    var url = apiUrl;
    IO.Socket socket = IO.io(url, <String, dynamic>{
      'transports': ['websocket']
    });

    socket.on('connect_error', (err) {
      print(err);
    });
    return socket;
  }

  void connectSocket() async {
    var url = apiUrl;

    IO.Socket socket = IO.io(url, <String, dynamic>{
      'transports': ['websocket']
    });
    socket.onConnect((_) {
      print('connect');
      connectionChecker = true;
      print(
          'valor de connection al conectarse ' + connectionChecker.toString());
      socket.emit('msg', 'test');
    });
    socket.onDisconnect(
      (_) => connectionChecker = false,
    );
    socket.on("connect_error", (data) => print('connect_error: ' + data));
  }
}
