import 'dart:async';
import 'package:bus_track_app/constants/colors.dart';
import 'package:bus_track_app/models/globals.dart';
import 'package:bus_track_app/screens/add_place_screen.dart';
import 'package:bus_track_app/screens/map_screen.dart';
import 'package:bus_track_app/screens/places_list_screen.dart';
import 'package:bus_track_app/services/service_locator.dart';
import 'package:bus_track_app/widgets/app_drawer.dart';
import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:page_transition/page_transition.dart';
import './models/route.dart' as BusRoute;

void main() {
  setupServiceLocator();
  runApp(MyApp());
}

// ignore: must_be_immutable
// class ConnectionAlertaDialog extends StatelessWidget {
//   bool revisando;
//   ConnectionAlertaDialog({this.revisando});

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       decoration: BoxDecoration(
//         color: const Color(0xff7c94b6),
//         image: new DecorationImage(
//           fit: BoxFit.cover,
//           colorFilter: ColorFilter.mode(
//               Colors.black.withOpacity(0.5), BlendMode.dstATop),
//           image: AssetImage("assets/images/backgroundGradiant.png"),
//         ),
//       ),
//       child: Text('probando coneccion'),
//     );
//   }
// }

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'ATUSUM'),
      routes: {
        AddPlaceScreen.routeName: (ctx) => AddPlaceScreen(),
        PlacesListScreen.routeName: (ctx) => PlacesListScreen(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  
  Future<Widget> loadFromFuture() async {
    // <fetch data from server. ex. login>
    return Future.value(new AfterSplash());
  }

  @override
  Widget build(BuildContext context) {
    return new AnimatedSplashScreen(
        splash: 'assets/images/animationBus.png',
        duration: 2000,
        splashTransition: SplashTransition.slideTransition,
        pageTransitionType: PageTransitionType.bottomToTop,
        animationDuration: Duration(milliseconds: 3000),
        // animationDuration: Duration(),
        nextScreen: LoadingAfterSplash());
  }

  @override
  void initState() {
    super.initState();
    tiempoDiferencia = 6;
  }
}

class LoadingAfterSplash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
      seconds: 4,
      navigateAfterSeconds: AfterSplash(),
      // navigateAfterFuture: loadFromFuture(),
      title: new Text(
        'BUS TRACK APP',
        style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
      ),
      image: new Image.network(
          'https://usercontent.one/wp/www.raulferrergarcia.com/wp-content/uploads/2020/11/Google-flutter-logo.png'),
      backgroundColor: Colors.white,
      styleTextUnderTheLoader: new TextStyle(),
      photoSize: 100.0,
      loadingText: Text('Espere mientras establecemos una conexión.'),
      onClick: () => print("Flutter"),
      loaderColor: Colors.red,
    );
  }
}

// ignore: must_be_immutable
class AfterSplash extends StatelessWidget {
  BusRoute.Route ruta;
  AfterSplash({this.ruta});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ATUSUM'),
        backgroundColor: vinoTinto,
      ),
      drawer: AppDrawer(),
      floatingActionButton: Container(
        
        // margin: new EdgeInsets.fromLTRB(20, 15, 20, 15),
        height: 40,
        width: 40,
        // padding: new EdgeInsets.fromLTRB(10, 0, 5, 0),
        child: ElevatedButton(
          style: ButtonStyle(
              alignment: Alignment.center,
              backgroundColor: MaterialStateProperty.all(rojoFuerte),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                      side: BorderSide(color: rojoFuerte)))),
          onPressed: () {
            Navigator.of(context).pushNamed(PlacesListScreen.routeName);
          },
          child: Text(
            '!',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
            textAlign: TextAlign.center,
          ),
        ),
      ),
      body: MapScreen(route: ruta),
    );
  }
}
