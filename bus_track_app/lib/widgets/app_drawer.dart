import 'package:bus_track_app/constants/colors.dart';
import 'package:bus_track_app/helpers/config.dart';
import 'package:bus_track_app/main.dart';
import 'package:bus_track_app/models/globals.dart';
import 'package:bus_track_app/screens/publicidad_screen.dart';
import 'package:google_fonts/google_fonts.dart';
// import 'package:bus_track_app/models/route.dart';
import '../models/route.dart' as BusRoute;
import 'package:bus_track_app/widgets/rutasListTile.dart';
import 'package:flutter/material.dart';
import '../helpers/rutas_helper.dart';

class AppDrawer extends StatefulWidget {
  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  @override
  void initState() {
    fetchPost().then((value) {
      setState(() {
        _listaRutas.addAll(value);
        _listaRutasDisplay = _listaRutas;
      });
    });
    print(
        'valor de horaDeUltimaPublicidad' + horaDeUltimaPublicidad.toString());
    print('valor de diferencia de ultima publicidad ' +
        tiempoDiferencia.toString());
    super.initState();
  }

  List<BusRoute.Route> _listaRutas = [];
  List<BusRoute.Route> _listaRutasDisplay = [];

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child:
            // _listaRutasDisplay.length > 0
            //     ?
            Column(
      children: <Widget>[
        AppBar(
          title: Text(
            'Rutas disponibles',
            style: GoogleFonts.openSans(
              textStyle: TextStyle(
                fontFamily: 'Open Sans',
                fontSize: bodyFont,
              ),
            ),
          ),
          automaticallyImplyLeading: false,
          backgroundColor: vinoTinto,
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(8, 15, 8, 0),
          child: SizedBox(
            height: 50,
            width: 300,
            child: AbsorbPointer(
              absorbing: _listaRutasDisplay.length >= 1 ? false : true,
              child: TextField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  labelStyle: TextStyle(color: darkGrey),
                  labelText: 'Buscar Ruta:',
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey),
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: vinoTinto),
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    ),
                  ),
                  prefixIcon: Icon(
                    Icons.search,
                    color: Colors.grey,
                  ),
                ),
                onChanged: (text) {
                  text = text.toLowerCase();
                  setState(() {
                    _listaRutasDisplay = _listaRutas.where(
                      (listaRutas) {
                        var rutaTitle = listaRutas.name.toLowerCase();
                        var placesTitle = [...listaRutas.places];

                        if (placesTitle.length > 0) {
                          for (int i = 0; i < placesTitle.length; i++) {
                            if (placesTitle[i].toLowerCase().contains(text)) {
                              return placesTitle[i]
                                  .toLowerCase()
                                  .contains(text);
                            } else {
                              continue;
                            }
                          }
                        }
                        return rutaTitle.contains(text);
                      },
                    ).toList();
                  });
                },
              ),
            ),
          ),
        ),
        Padding(
            padding: EdgeInsets.fromLTRB(8, 5, 8, 0),
            child: FlatButton(
                onPressed: () {
                  setState(() {
                    isSelecting = true;
                    print('valor de isselecting appdrawer = ' +
                        isSelecting.toString());
                    print('valor de isselecting appdrawer = ' +
                        isSelecting.toString());
                    // Navigator.pop(context);
                    Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (context) => AfterSplash()),
                      (Route<dynamic> route) => false,
                    );
                  });
                },
                child: Text('Fijar Ubicacion en el mapa'))),
        Expanded(
          child: _listaRutasDisplay.length > 0
              ? ListView.builder(
                  itemCount: _listaRutasDisplay.length,
                  itemBuilder: (ctx, i) => GestureDetector(
                    onTap: () {
                      tiempoDiferencia = DateTime.now()
                          .difference(horaDeUltimaPublicidad)
                          .inMinutes;
                      print(
                        'lepico a la ruta = ' +
                            _listaRutasDisplay[i].name.toString() +
                            'y el valor de tiempodif es = ' +
                            tiempoDiferencia.toString(),
                      );
                      if (tiempoDiferencia > 4 || tiempoDiferencia == null) {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => PublicidadScreen(
                              route: _listaRutasDisplay[i],
                            ),
                          ),
                        );
                      } else {
                        print('no entro porque tiempodif es = ' +
                            tiempoDiferencia.toString());
                        Navigator.pop(context);
                        Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                            builder: (context) => AfterSplash(
                              ruta: _listaRutasDisplay[i],
                            ),
                          ),
                        );
                      }
                    },
                    child: RutasList(
                        _listaRutasDisplay[i].name.toString(),
                        _listaRutasDisplay[i].places.length > 0
                            ? _listaRutasDisplay[i].places.join(", ")
                            : 'Por ahora esta ruta no tiene lugares asignados'),
                  ),
                )
              : Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(
                        child: Icon(
                          Icons.railway_alert,
                          size: 40,
                          color: vinoTinto,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: new Container(
                                margin: const EdgeInsets.only(
                                    left: 0.0, right: 5.0),
                                child: Divider(
                                  color: Colors.black,
                                  height: 25,
                                )),
                          ),
                          SizedBox(
                            width: 110,
                            child: Text(
                              'No se encontraron rutas disponibles en este momento',
                              style: GoogleFonts.openSans(
                                textStyle: TextStyle(
                                    fontFamily: 'Open Sans',
                                    fontSize: font15,
                                    color: Colors.grey),
                              ),
                            ),
                          ),
                          Expanded(
                            child: new Container(
                              margin:
                                  const EdgeInsets.only(left: 5.0, right: 0.0),
                              child: Divider(
                                color: Colors.black,
                                height: 25,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      SizedBox(
                        width: 110,
                        child: FloatingActionButton(
                          child: Icon(Icons.replay_outlined),
                          backgroundColor: vinoTinto,
                          onPressed: () {
                            fetchPost().then((value) {
                              setState(() {
                                _listaRutas.addAll(value);
                                _listaRutasDisplay = _listaRutas;
                              });
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                ),
        ),
      ],
    )
        // : Center(
        //     child: CircularProgressIndicator(),
        //   ),
        );
  }
}
