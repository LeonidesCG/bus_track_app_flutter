import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
// import 'package:path/path.dart' as path;
// import 'package:path_provider/path_provider.dart' as syspaths;

class ImageInput extends StatefulWidget {
  final Function onSelectImage;
  ImageInput(this.onSelectImage);

  @override
  _ImageInputState createState() => _ImageInputState();
}

class _ImageInputState extends State<ImageInput> {
  File _storedImage;
  double containerWidth = 150;
  double containerHeight = 100;
  bool bandera = false;
  bool esImagenDeGaleria = true;
  bool esImagenDeCamara = true;
  Future<void> _takePicture() async {
    final picker = ImagePicker();
    final imageFile =
        await picker.getImage(source: ImageSource.camera, maxWidth: 600);
    setState(() {
      _storedImage = File(imageFile.path);
      esImagenDeGaleria = false;
    });
    // final appDir = await syspaths.getApplicationDocumentsDirectory();
    // final fileName = path.basename(imageFile.path);
    // final savedImage = await _storedImage.copy('${appDir}/$fileName');
    widget.onSelectImage(_storedImage);
  }

  Future<void> _imgFromGallery() async {
    //ignore: deprecated_member_use
    PickedFile imageFile = await ImagePicker().getImage(source: ImageSource.gallery, maxWidth: 600);
        // ignore: deprecated_member_use
        // await ImagePicker.(source: ImageSource.gallery, maxWidth: 600);


    setState(() {
      _storedImage = File(imageFile.path);
      esImagenDeCamara = false;
    });
    widget.onSelectImage(_storedImage);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        esImagenDeCamara == true
            ? GestureDetector(
                child: Center(
                  child: Container(
                    width: _storedImage != null ? 330 : containerWidth,
                    height: _storedImage != null ? 330 : containerHeight,
                    decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Colors.grey),
                      borderRadius: BorderRadius.all(
                        Radius.circular(5),
                      ),
                    ),
                    child: _storedImage != null
                        ? Image.file(
                            _storedImage,
                            fit: BoxFit.cover,
                            width: double.infinity,
                          )
                        : GestureDetector(
                            onTap: _takePicture,
                            // ignore: deprecated_member_use
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 20,
                                ),
                                Icon(
                                  Icons.camera_alt_outlined,
                                  color: Colors.black54,
                                ),
                                Text(
                                  'Tomar una foto',
                                  style: GoogleFonts.openSans(
                                    textStyle: TextStyle(
                                      fontFamily: 'Open Sans',
                                      fontSize: 15,
                                    ),
                                  ),
                                ),
                              ],
                            )),
                    alignment: Alignment.center,
                  ),
                ),
              )
            : SizedBox(
                height: 0,
                width: 0,
              ),
        SizedBox(
          width: _storedImage != null ? 0 : 40,
        ),
        esImagenDeGaleria == true
            ? GestureDetector(
                child: Center(
                  child: Container(
                    width: _storedImage != null ? 330 : containerWidth,
                    height: _storedImage != null ? 330 : containerHeight,
                    decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Colors.grey),
                      borderRadius: BorderRadius.all(
                        Radius.circular(5),
                      ),
                    ),
                    child: _storedImage != null
                        ? Stack(
                            children: [
                              Image.file(
                                _storedImage,
                                fit: BoxFit.cover,
                                width: double.infinity,
                              ),
                              Align(
                                alignment: Alignment.topRight,
                                child: TextButton.icon(
                                  icon: Icon(
                                    Icons.delete,
                                    color: Colors.grey,
                                  ),
                                  label: Text(
                                    'Borrar foto',
                                    style: GoogleFonts.openSans(
                                      textStyle: TextStyle(
                                          fontFamily: 'Open Sans',
                                          fontSize: 15,
                                          color: Colors.grey,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      _storedImage = null;
                                      esImagenDeGaleria = true;
                                      esImagenDeCamara = true;
                                    });
                                  },
                                ),
                              )
                            ],
                          )
                        : GestureDetector(
                            onTap: _imgFromGallery,
                            // ignore: deprecated_member_use
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 20,
                                ),
                                Icon(
                                  Icons.image_outlined,
                                  color: Colors.black54,
                                ),
                                Text(
                                  'Subir una foto',
                                  style: GoogleFonts.openSans(
                                    textStyle: TextStyle(
                                      fontFamily: 'Open Sans',
                                      fontSize: 15,
                                    ),
                                  ),
                                ),
                              ],
                            )),
                    alignment: Alignment.center,
                  ),
                ),
              )
            : SizedBox(
                height: 0,
                width: 0,
              ),

        // !bandera
        //     ? Expanded(
        //         // ignore: deprecated_member_use
        //         child: FlatButton.icon(
        //           icon: Icon(
        //             Icons.camera_alt_outlined,
        //             color: Colors.black54,
        //           ),
        //           label: Text(
        //             'Tomar una foto',
        //             style: GoogleFonts.openSans(
        //               textStyle: TextStyle(
        //                 fontFamily: 'Open Sans',
        //                 fontSize: 15,
        //               ),
        //             ),
        //           ),
        //           onPressed: _takePicture,
        //         ),
        //       )
        //     : SizedBox(
        //         height: 0,
        //         width: 0,
        //       ),
      ],
    );
  }
}
