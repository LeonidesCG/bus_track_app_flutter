import 'package:bus_track_app/constants/colors.dart';
import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';

class RutasList extends StatelessWidget {
  final String nombreRuta;
  final String lugaresRuta;

  RutasList(this.nombreRuta, this.lugaresRuta);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: Colors.grey[200]),
        ),
      ),
      width: double.infinity,
      margin: const EdgeInsets.only(left: 0.0, right: 0.0),
      child: ListTile(
        title: Text(
          nombreRuta,
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        subtitle: Text(lugaresRuta),
        trailing: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 3, 3, 0),
              child: Icon(
                CommunityMaterialIcons.chart_line_variant,
                color: Colors.black,
                size: 40,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(6, 2, 0, 0),
              child: Icon(
                Icons.directions_bus,
                color: vinoTinto,
                size: 25,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
